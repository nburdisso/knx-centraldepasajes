﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Configuration;

namespace CentralDePasajes.Controllers
{
    public class HomeController : Controller
    {
        [Route ("experiencias/{url_paquete}")]
        public ActionResult Index(string url_paquete)
        {
            //building the json Url
            var urlBuilder =
            new System.UriBuilder(Request.Url.AbsoluteUri)
            {
                Path = Url.Content("~/Content/Paquetes.json"),
                Query = null,
            };

            Uri uri = urlBuilder.Uri;
            string url = urlBuilder.ToString();

            // getting the jsonData
            var paquetes = getJSONData<List<PaqueteJson>>(url);

            // filtering the data by url
            var model_paquete = new PaqueteJson();

            foreach (var _paquete in paquetes) {
                if( url_paquete == _paquete.url) {
                    model_paquete = _paquete;
                };
            }

            return View(model_paquete);
        }

        [Route("SendMail")]
        [HttpPost]
        public ActionResult SendMail(EmailData emaildata)
        {
            var recaptchaResponse = Request["g-recaptcha-response"];

            if (!reCaptcha.Validate(recaptchaResponse)) {
                return Json(new { success = false, responseText = "El recaptcha no es válido, intente de nuevo." }, JsonRequestBehavior.AllowGet);
            };

            try
            {
                MailMessage tmail = new MailMessage();

                // User information
                StringBuilder builder = new StringBuilder();

                builder.Append(string.Format("Nombre: {0} <br/><br/>", emaildata.name));
                if (!String.IsNullOrEmpty(emaildata.mail))
                {
                    builder.Append(string.Format("Email: {0} <br/><br/>", emaildata.mail));
                };
                if (!String.IsNullOrEmpty(emaildata.area))
                {
                    builder.Append(string.Format("Cod Area: {0} <br/><br/>", emaildata.area));
                };
                if (!String.IsNullOrEmpty(emaildata.phone))
                {
                    builder.Append(string.Format("Teléfono: {0} <br/><br/>", emaildata.phone));
                };
                if (!String.IsNullOrEmpty(emaildata.schedule))
                {
                    builder.Append(string.Format("Horario de contacto: {0} <br/><br/>", emaildata.schedule));
                };

                // Package information
                builder.Append("<b>Información del paquete:</b><br/><br/>");
                builder.Append(string.Format("  <b>Salida:</b> {0} <br/> ", emaildata.salida));
                builder.Append(string.Format("   <b>País:</b> {0} <br/> ", emaildata.pais));
                builder.Append(string.Format("   {0}<br/> ", emaildata.salidas));
                builder.Append(string.Format("   <b>Duración:</b> {0} <br/> ", emaildata.duracion));
                builder.Append(string.Format("   <b>Financiación:</b> {0} <br/> ", emaildata.financiation));
                builder.Append(string.Format("   <b>Condiciones:</b> {0} <br/> ", emaildata.conditions));
                builder.Append(string.Format("   <b>Precio:</b> ${0} <br/> ", emaildata.price));
                builder.Append(string.Format("   <b>Adicionales:</b>  {0} <br/> ", emaildata.iconos));

                // Mail information
                tmail.From = new MailAddress(ConfigurationManager.AppSettings["FromMail"]);
                tmail.To.Add(new MailAddress(ConfigurationManager.AppSettings["ContactMail"]));
                tmail.Subject = "Contacto";
                tmail.Body = builder.ToString();
                tmail.IsBodyHtml = true;
                tmail.Priority = MailPriority.Normal;
                var smtp = new SmtpClient();
                {
                    smtp.Host = ConfigurationManager.AppSettings["smtpHost"];
                    smtp.Port = 587;
                    smtp.EnableSsl = false;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["FromMail"], ConfigurationManager.AppSettings["PasswordCorreo"]);
                }
                smtp.Send(tmail);

                return Json(new { success = false, responseText = "Su mail ha sido enviado con éxito!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                return Json(new { success = true, responseText = "Ha ocurrido un error en el envío del mail!" }, JsonRequestBehavior.AllowGet);
            }


        }

        private static T getJSONData<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }
    }
}