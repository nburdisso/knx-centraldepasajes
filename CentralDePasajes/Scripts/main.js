﻿var recaptchaKey = "6LcVjD4UAAAAALP7wFSogT_sO8qjl2r8_3vZVkJ0";
var recaptchaPhone = {};
var recaptchaMail = {};

function CaptchaCallback() {
    recaptchaPhone = grecaptcha.render('captcha-phone', {'sitekey': recaptchaKey});
    recaptchaMail = grecaptcha.render('captcha-mail', {'sitekey': recaptchaKey});
}

function FormValidation(formInstance) {
    if ($('#option-phone-container').hasClass("hidden")) {
        var captcha = $("#captcha-mail");
        var captchaParsley = captcha.parsley();
        window.ParsleyUI.removeError(captchaParsley, "captchaError");
        var key = grecaptcha.getResponse(recaptchaMail);
        captcha.val(key);
        if (!captcha.val()) {
            window.ParsleyUI.addError(captchaParsley, "captchaError", 'Resuelva el captcha');
            formInstance.validationResult = false;
        }
    } else {
        var captcha = $("#captcha-phone");
        var captchaParsley = captcha.parsley();
        window.ParsleyUI.removeError(captchaParsley, "captchaError");
        var key = grecaptcha.getResponse(recaptchaPhone);
        captcha.val(key);
        if (!captcha.val()) {
            window.ParsleyUI.addError(captchaParsley, "captchaError", 'Resuelva el captcha');
            formInstance.validationResult = false;
        }
    }
}

$(document).ready(function () {
    window.Parsley.addValidator("area", {
        validateString: function (value, requirement) {
            return /([0-9]{2,5})/.test(value);
        },
        messages: {
            es: "Ingresá un código de área con formato válido"
        }
    });

    window.Parsley.addValidator("telephone", {
        validateString: function (value, requirement) {
            return /([0-9]+)/.test(value);
        },
        messages: {
            es: "Ingresá un teléfono con formato válido"
        }
    });

    $("#option-phone-container").parsley().on('form:validate', FormValidation);

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('input[type="radio"][name="inlineRadioOptions"]').on('change', function (e) {
        $('input[type=text]').val("");
        $('input[type=email]').val("");
        $('select').val("");
        if (this.id == "option-phone") {
            $("#option-phone-container").removeClass("hidden");
            $("#option-mail-container").addClass("hidden");
            $("#option-mail-container").parsley().destroy();
            $("#option-phone-container").parsley().on('form:validate', FormValidation);

            grecaptcha.reset(recaptchaMail);
        }
        else if (this.id == "option-mail") {
            $("#option-phone-container").addClass("hidden");
            $("#option-mail-container").removeClass("hidden");
            $("#option-phone-container").parsley().destroy();
            $("#option-mail-container").parsley().on('form:validate', FormValidation);

            grecaptcha.reset(recaptchaPhone);
        }
    });  

    $('.cta-phone').affix({
        offset: {
            top: function () {
                return $('.pack-items').height() + 20
            }
        }
    });

    var isMobile = window.matchMedia("only screen and (max-width: 767px)");

    if (isMobile.matches) {
        $('.pack-container').css({height: ($(window).height() - 157)});
    }    

    $("#option-phone-container").submit(function(e) {      
        e.preventDefault();
        $.ajax({
            url: "/SendMail",
            type: "POST",
            dataType: 'json',
            data: $("#option-phone-container").serialize(),
            success: function (data) {
                alert(data.responseText);
                grecaptcha.reset();
            },
            error: function (data) {
                alert(data.responseText);
                grecaptcha.reset();
            },
        });
    });
    $("#option-mail-container").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "/SendMail",
            type: "POST",
            dataType: 'json',
            data: $("#option-mail-container").serialize(),
            success: function (data) {
                alert(data.responseText);
                grecaptcha.reset();
            },
            error: function (data) {
                alert(data.responseText);
                grecaptcha.reset();
            },
        });
    });



});