﻿public class PaqueteJson
{
    public string pais { get; set; }
    public string salida { get; set; }
    public string url { get; set; }
    public string duracion { get; set; }
    public string salidas { get; set; }
    public int precio { get; set; }
    public string condiciones { get; set; }
    public string financiacion { get; set; }
    public string horarios { get; set; }
    public Icono[] iconos { get; set; }
}

public class Icono
{
    public string logoClaro { get; set; }
    public string logoOscuro { get; set; }
    public string leyenda { get; set; }
}
