﻿using System.Collections.Generic;
using System.Web.Mvc;

public class EmailData{
    public string name { get; set; }
    public string area { get; set; }
    public string phone { get; set; }
    public string schedule { get; set; }
    public string mail { get; set; }
    public string salida { get; set; }
    public string pais { get; set; }
    [AllowHtml]
    public string salidas { get; set; }
    public string duracion { get; set; }
    public string financiation { get; set; }
    public string conditions { get; set; }
    public string price { get; set; }
    public string iconos { get; set; }
}


public class reCaptcha
{
    public bool Success { get; set; }
    public List<string> errorcodes { get; set; }

    public static bool Validate(string token)
    {
        var client = new System.Net.WebClient();

        string PrivateKey = "6LcVjD4UAAAAAA03T7igRyW2KEtHuJNVFfUDcBOj";

        var url = $"https://www.google.com/recaptcha/api/siteverify?secret={PrivateKey}&response={token}";
        var GoogleReply = client.DownloadString(url);

        var captchaResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<reCaptcha>(GoogleReply);

        return captchaResponse.Success;         
    }
}
